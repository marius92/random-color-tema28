<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CounterController extends AbstractController
{
    /**
     * @Route("/counter", name="counter")
     */
    public function index()
    {
        return $this->render('counter/index.html.twig', [
            'controller_name' => 'CounterController',
        ]);
    }
}
