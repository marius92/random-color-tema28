<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TemeController extends AbstractController
{
    /**
     * @Route("/", name="teme")
     */
    public function index()
    {
        return $this->render('teme/index.html.twig', [
            'controller_name' => 'TemeController',
        ]);
    }
}
