/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
require('bootstrap');
import Vue from 'vue';
import axios from 'axios';
var color = new Vue({
    el: '#color',
   delimiters: ['{*', '*}'],
    data: {
        mycolor: '#000000',
        back: '#000000'
    },
    methods: {
        generator: function () {
            this.mycolor = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
            this.mycolor2 = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
            return this.mycolor , this.mycolor2;
        },
        background: function () {
            this.back = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
            return document.body.style.background = this.back;
        }
    }
});
// const color = new Vue({
//     el: '#color',
//     delimiters: ['{*', '*}'],
//     data: {
//         range: [{
//             id: 1,
//             id: 2,
//             id: 3,
//             id: 4,
//             id: 5,
//             id: 6,
//             id: 7
//         }],
//         colorCache: {}
//     },
//     methods: {
//         randomColor(id) {
//             const r = () => Math.floor(256 * Math.random());
//             return this.colorCache[id] || (this.colorCache[id] = `rgb(${r()}, ${r()}, ${r()})`);
//         }
//     }

// });

