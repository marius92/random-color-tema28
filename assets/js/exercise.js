/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
require('bootstrap');
import Vue from 'vue';
var exercise = new Vue({
    el: '#exercise',
    delimiters: ['{*', '*}'],
    data: {
        name: 'Nastasa',
        surname: 'Marius',
        image: "https://images.pexels.com/photos/248797/pexels-photo-248797.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
        age: 27,


    },
    methods: {
        random: function () {
            var min = 0,
                max = 100,
                random = Math.random() * (max - min) + min;
            return random;
        },
        nameDisplay: function () {
            return this.name + " " + this.surname;
        }
    }

});