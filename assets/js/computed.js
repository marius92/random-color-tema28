/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
require('bootstrap');
import Vue from 'vue';
import axios from 'axios';
var computed = new Vue({
    el: '#computed',
    delimiters: ['{*', '*}'],
    data: {
        nume: 'Nastasa',
        prenume: 'Marius',
        findName: "",
        jsonObj: null,
        myWatch: "neschimbat",
        names: [
            "George",
            "Marius",
            "Denisa",
            "Alin",
            "Naomi",
            "Edi",
            "Timi",
            "Simona"
        ],

    },
    computed: {
        numeIntreg: function () {
            return this.nume + " " + this.prenume;
        },
        search: function () {
            var searchText = new RegExp(this.findName, "i");
            return this.names.filter(el => el.match(searchText));
        },
        searchJson: function () {
            var searchText = new RegExp(this.findName, "i");
            return this.jsonObj.data.names.filter(el => el.match(searchText));
        }
    },
    mounted: function () {
        axios
            .get('http://curs-21.local/namelist')
            .then(response => (this.jsonObj = response))
    },
    watch: {
        findName:function(value) {
        console.log("findName has been changed" + value);
            this.myWatch = value;
            // this.nume = value;
    }
}
});
